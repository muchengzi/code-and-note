meta标签：用来描述html文件的辅助标签
  一、meta标签的组成  
    meta标签共有两个属性，它们分别是http-equiv属性和name属性，不同的属性又有不同的参数值，这些不同的参数值就实现了不同的网页功能。  
    1、name属性  
    name属性主要用于描述网页，与之对应的属性值为content，content中的内容主要是便于搜索引擎机器人查找信息和分类信息用的。  
    meat标签的name属性语法格式是：＜meta name="参数" content="具体的参数值"＞ 。  
    其中name属性主要有以下几种参数：  
    A、Keywords(关键字)      
    B、description(网站内容描述)   
    C、robots(机器人向导)    
    D、author(作者)  
   二、http-equiv属性
    http的文件头作用，以帮助正确和精确地显示网页内容，与之对应的属性值为content，content中的内容其实就是各个参数的变量值。meat标签的http-equiv属性语法格式是：<meta http-equiv="参数" content="参数变量值" />
    A、Expires(期限)
     说明：可以用于设定网页的到期时间。一旦网页过期，必须到服务器上重新传输。
    B、Pragma(cache模式)
     说明：禁止浏览器从本地计算机的缓存中访问页面内容。   
    C、Refresh(刷新)
     说明：自动刷新并指向新页面。
    D、Set-Cookie(cookie设定)
     说明：如果网页过期，那么存盘的cookie将被删除。
总结：使用meta标签用来描述项目的显示和在搜索引擎的查询。

bootstrap的引入：
两种方式：官网的cdn链接和在官网下载下来后link

